import { Component, OnInit } from '@angular/core';
import { Camera , CameraOptions, DestinationType, EncodingType } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.page.html',
  styleUrls: ['./camera.page.scss'],
})
export class CameraPage{
  imagem: string; //base64

  options: CameraOptions = { 
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  constructor(
    private camera: Camera
    ) {   }

  ngOnInit() {
  }

  tiraFoto(){
    this.camera.getPicture(this.options).then(ImageData => {
      const base64Image = 'data:image/jpeg;base64,' + ImageData;
      this.imagem = base64Image;
    });
  }
}
